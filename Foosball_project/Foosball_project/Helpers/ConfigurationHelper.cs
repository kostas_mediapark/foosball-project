﻿using Microsoft.Extensions.Configuration;
using System;

namespace Foosball_project.Helpers
{
    public static class ConfigurationHelper
    {
        private const string DEFAULT_CONNECTION = "DefaultConnection";

        public static string GetDefaultConnection(this IConfigurationRoot config) => config.GetConnectionString(DEFAULT_CONNECTION);

        /// <summary>
        /// Separator is :
        /// </summary>
        public static T Get<T>(this IConfigurationRoot config, string key) where T : IConvertible => ConvertTo<T>(config[key]);

        public static T Get<T>(this IConfigurationRoot config, params string[] keys) where T : IConvertible => Get<T>(config, string.Join(':', keys));

        private static T ConvertTo<T>(IConvertible obj) where T : IConvertible => (T)ConvertTo(obj, typeof(T));

        private static object ConvertTo(IConvertible obj, Type convertTo)
        {
            Type t = obj.GetType();

            // T is nullable
            if (convertTo != null)
                return (obj == null) ? convertTo.GetDefaultValue() : Convert.ChangeType(obj, convertTo);

            // T is not nullable
            else
                return Convert.ChangeType(obj, convertTo);
        }

        private static object GetDefaultValue(this Type t) => t.IsValueType ? Activator.CreateInstance(t) : null;
    }
}
