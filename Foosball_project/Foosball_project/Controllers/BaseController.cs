﻿using Foosball_project.Database;
using Foosball_project.Database.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;

namespace Foosball_project.Controllers
{
    /// <summary>
    /// Base controller
    /// </summary>
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// Services
        /// </summary>
        protected readonly IServiceProvider _services;

        /// <summary>
        /// Logger
        /// </summary>
        protected readonly ILogger _logger;

        /// <summary>
        /// Environment
        /// </summary>
        protected readonly IHostingEnvironment _env;
        
        /// <summary>
        /// Memory chach
        /// </summary>
        protected readonly IMemoryCache _cache;

        private MemoryCacheEntryOptions cacheEntryOptions = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(5));

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public BaseController(IServiceProvider services, IMemoryCache cache)
        {
            _services = services;
        }

        /// <summary>
        /// Return current user session token
        /// </summary>
        [NonAction]
        public SessionToken GetCurrentUserSessionToken() => GetCurrentUserSessionTokenIdentity()?.SessionToken;

        /// <summary>
        /// Get Identity. Used all over the controller.
        /// </summary>
        [NonAction]
        public SessionTokenIdentity GetCurrentUserSessionTokenIdentity()
        {
            var identity = HttpContext?.User?.Identity;

            if (identity == null)
                return null;

            if (!(identity is SessionTokenIdentity))
                return null;

            var sesionTokenIdentity = (SessionTokenIdentity)identity;

            return sesionTokenIdentity;
        }

        /// <summary>
        /// Returns current user id
        /// </summary>
        [NonAction]
        public int? GetCurrentUserId() => GetCurrentUserSessionToken()?.UserId;
    }
}
