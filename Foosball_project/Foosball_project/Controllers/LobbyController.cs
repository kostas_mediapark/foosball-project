﻿using Foosball_project.Database.Interfaces;
using Foosball_project.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Controllers
{
    [Route("api/[controller]")]
    [SessionTokenAuthorize]
    [ApiController]
    public class LobbyController : BaseController
    {
        private readonly ILobbyRepository _lobbyRepository;
        private readonly IPlayerRepository _playerRepository;

        public LobbyController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _lobbyRepository = services.GetRequiredService<ILobbyRepository>();
            _playerRepository = services.GetRequiredService<IPlayerRepository>();
        }

        [HttpGet]
        public async Task<IActionResult> GetLobby() => Ok(await _lobbyRepository.Get());

        [HttpPost("join")]
        public async Task<IActionResult> AddPlayer()
        {
            return Ok(await _lobbyRepository.AddPlayer(GetCurrentUserId().Value));
        }

        [HttpPost("leave")]
        public async Task<IActionResult> RemovePlayer()
        {
            return Ok(await _lobbyRepository.RemovePlayer(GetCurrentUserId().Value));
        }

        [HttpPost("end")]
        public async Task<IActionResult> EndGame()
        {
            var lobby = await _lobbyRepository.Get();
            var currentUser = await _playerRepository.GetById(GetCurrentUserId().Value);

            if (lobby.Players.All(x => x != currentUser.Username))
                return BadRequest("This user can't end game");

            await _lobbyRepository.ScheduledEndGame();

            return Ok();
        }
    }
}
