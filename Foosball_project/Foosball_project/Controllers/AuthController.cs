﻿using System;
using System.Threading.Tasks;
using Foosball_project.Database.Models;
using Foosball_project.Services;
using Foosball_project.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Foosball_project.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : BaseController
    {
        private readonly IAuthService _authService;

        public AuthController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _authService = services.GetAuthService();
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]LoginModel login) => Ok(new TokenModel(await _authService.Login(login)));

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegistrationModel register)
        {
            return await _authService.Register(register);
        }
    }
}
