﻿using Foosball_project.Database.Enums;
using Foosball_project.Database.Interfaces;
using Foosball_project.Database.Models;
using Foosball_project.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace Foosball_project.Services
{
    public class AuthService : BaseService, IAuthService
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly ISessionTokenService _sessionTokenService;
        private readonly IPasswordService _passwordService;

        public AuthService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _sessionTokenService = services.GetSessionTokenService();
            _playerRepository = services.GetPlayerRepository();
            _passwordService = services.GetPasswordService();
        }

        public async Task<Tuple<string, SessionToken>> Login(LoginModel login)
        {
            login.CleanUpInputs();

            var user = await _playerRepository.Get(login.Email)
                ?? throw new Exception("No user found.");

            var isValid = _passwordService.ValidatePassword(login.Password, user.Password);

            if (!isValid)
                throw new UnauthorizedAccessException("Invalid password!");

            return Tuple.Create(user.Username, await _sessionTokenService.GenerateToken(user.Id));
        }

        public async Task<IActionResult> Register (RegistrationModel registration)
        {
            if (await _playerRepository.Get(registration.Email) != null)
                return new ConflictObjectResult(new Exception("This e-mail already exists."));

            if (await _playerRepository.GetByUserName(registration.UserName) != null)
                return new ConflictObjectResult(new Exception("This username already exists."));

            if (registration.IsValidInformation())
            {
                var user = new PlayerModel()
                {
                    Email = registration.Email,
                    Status = LobbyStatus.Empty,
                    TableId = null,
                    Username = registration.UserName
                };

                user.Password = _passwordService.HashPassword(registration.Password);

                await _playerRepository.Add(user);

                return new OkResult();
            }

            return new BadRequestObjectResult(new Exception("Invalid registration information provided."));
        }
    }
}