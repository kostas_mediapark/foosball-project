﻿using Foosball_project.Configuration;
using Foosball_project.Database.Interfaces;
using Foosball_project.Repositories;
using Foosball_project.Services.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Security.Cryptography;

namespace Foosball_project.Services
{
    public static class ServiceExtensions
    {
        // Services
        public static ISessionTokenService GetSessionTokenService(this IServiceProvider services) => services.GetRequiredService<ISessionTokenService>();
        public static IAuthService GetAuthService(this IServiceProvider services) => services.GetRequiredService<IAuthService>();
        public static IRngService GetRngService(this IServiceProvider services) => services.GetRequiredService<IRngService>();
        public static IPasswordService GetPasswordService(this IServiceProvider services) => services.GetRequiredService<IPasswordService>();

        //Repositories
        public static IPlayerRepository GetPlayerRepository(this IServiceProvider services) => services.GetRequiredService<IPlayerRepository>();
        public static ISessionTokenRepository GetSessionTokenRepository(this IServiceProvider services) => services.GetRequiredService<ISessionTokenRepository>();

        // Other
        public static ConfigurationSettings GetConfig(this IServiceProvider services) => services.GetRequiredService<ConfigurationSettings>();
        public static RNGCryptoServiceProvider GetRngCrypto(this IServiceProvider service) => service.GetRequiredService<RNGCryptoServiceProvider>();

        // Called on API startup. DI configuration.
        public static void ConfigureServices(IServiceCollection services, IConfigurationRoot config)
        {
            // Services
            services.AddScoped<ISessionTokenService, SessionTokenService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IRngService, RngService>();
            services.AddScoped<IPasswordService, PasswordService>();

            // Repositories
            services.AddScoped<IBaseRepository, BaseRepository>();
            services.AddScoped<IPlayerRepository, PlayerRepository>();
            services.AddScoped<ISessionTokenRepository, SessionTokenRepository>();
            services.AddScoped<ILobbyRepository, LobbyRepository>();

            // Other
            services.AddSingleton<RNGCryptoServiceProvider>();
        }
    }
}
