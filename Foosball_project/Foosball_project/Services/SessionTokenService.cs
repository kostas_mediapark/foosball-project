﻿using Foosball_project.Database.Interfaces;
using Foosball_project.Database.Models;
using Foosball_project.Services.Contracts;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wiry.Base32;

namespace Foosball_project.Services
{
    public class SessionTokenService : BaseService, ISessionTokenService
    {
        private readonly ISessionTokenRepository _sessionTokenRepository;

        public SessionTokenService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _sessionTokenRepository = services.GetSessionTokenRepository();
        }

        public bool IsValidFormat(string tokenString) => tokenString.Count(v => v == '.') == 2;

        public async Task<SessionToken> GenerateToken(int userId)
        {
            var token = new SessionToken()
            {
                UserId = userId
            };

            token.Token = GenerateTokenString(userId);
            
            //await LogoutClient(userId);
            var newToken = await _sessionTokenRepository.Add(token);

            return newToken;
        }

        private string GenerateTokenString(int userId, string phoneNumber = "")
        {
            byte[] randomBytes = _services.GetRngService().Bytes(_config.SessionToken.ByteCount);

            var tokenString = Base32Encoding.Standard.GetString(randomBytes);

            var userIdEncoded = Base32Encoding.Standard.GetString(BitConverter.GetBytes(userId));

            var phoneNumberEncoded = Base32Encoding.Standard.GetString(Encoding.Unicode.GetBytes(phoneNumber));

            return $"{tokenString}.{userIdEncoded}.{phoneNumberEncoded}";
        }

        public int ParseUserId(string tokenString)
            => BitConverter.ToInt32(Base32Encoding.Standard.ToBytes(tokenString.Split('.')[1]), 0);
    }
}
