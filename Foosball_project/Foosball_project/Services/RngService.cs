﻿using Foosball_project.Services.Contracts;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Security.Cryptography;

namespace Foosball_project.Services
{
    public class RngService : BaseService, IRngService
    {
        private readonly RNGCryptoServiceProvider _rng;

        public RngService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache) => _rng = services.GetRngCrypto();

        public byte[] Bytes(int size)
        {
            var bytes = new byte[size];

            _rng.GetBytes(bytes);

            return bytes;
        }
    }
}
