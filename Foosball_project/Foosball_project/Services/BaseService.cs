﻿using Foosball_project.Configuration;
using Microsoft.Extensions.Caching.Memory;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Foosball_project.Services
{
    public abstract class BaseService
    {
        protected readonly IServiceProvider _services;
        protected readonly ConfigurationSettings _config;
        protected readonly IMemoryCache _memoryCache;

        public BaseService(IServiceProvider serviceProvider, IMemoryCache memoryCache)
        {
            _services = serviceProvider;
            _config = serviceProvider.GetService<ConfigurationSettings>();
            _memoryCache = memoryCache;
        }
    }
}
