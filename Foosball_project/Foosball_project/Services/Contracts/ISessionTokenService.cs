﻿using Foosball_project.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Services.Contracts
{
    public interface ISessionTokenService
    {
        /// <summary>
        /// Parses the given token. Extracts and returns the user ID found within the parsed string.
        /// </summary>
        /// <param name="tokenString"></param>
        /// <returns></returns>
        int ParseUserId(string tokenString);

        /// <summary>
        /// Checks and returns a bool value whether the token provided is formed in a valid format.
        /// </summary>
        /// <param name="tokenString"></param>
        /// <returns></returns>
        bool IsValidFormat(string tokenString);
 
        /// <summary>
        /// Generates and returns a session token for the web application of administrator user type.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<SessionToken> GenerateToken(int userId);
    }
}
