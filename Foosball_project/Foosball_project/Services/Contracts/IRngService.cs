﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Services.Contracts
{
    public interface IRngService
    {
        /// <summary>
        /// Generates a random array of bytes.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        byte[] Bytes(int size);
    }
}
