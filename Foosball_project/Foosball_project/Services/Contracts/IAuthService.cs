﻿using Foosball_project.Database.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Foosball_project.Services.Contracts
{
    public interface IAuthService
    {
        /// <summary>
        /// Returns a new session token if the provided login model details are valid.
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        Task<Tuple<string, SessionToken>> Login(LoginModel login);

        /// <summary>
        /// Returns a new user if the provided registration details are correct
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        Task<IActionResult> Register(RegistrationModel registration);
    }
}
