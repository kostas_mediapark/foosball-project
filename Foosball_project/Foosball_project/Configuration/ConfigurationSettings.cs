﻿namespace Foosball_project.Configuration
{
    public class ConfigurationSettings
    {
        public GeneralConfig General { get; set; }
        public CorsConfig Cors { get; set; }
        public SessionTokenConfig SessionToken { get; set; }
    }

    public class GeneralConfig
    {
        public string Environment { get; set; }
        public string AzureUrl { get; set; }
        public string BasicAuth { get; set; }

        public bool IsProduction() => Environment == "Production";
        public bool IsDevelopment() => Environment == "Development";
        public bool IsLocal() => Environment == "Local";
    }

    public class SessionTokenConfig
    {
        public decimal LifetimeMinutes { get; set; }

        public int ByteCount { get; set; }
    }

    public class CorsConfig
    {
        public string[] Origins { get; set; }
    }
}
