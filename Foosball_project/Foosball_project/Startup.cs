﻿using Foosball_project.Attributes;
using Foosball_project.Configuration;
using Foosball_project.Database;
using Foosball_project.Database.Interfaces;
using Foosball_project.Filters;
using Foosball_project.Helpers;
using Foosball_project.Services;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Foosball_project
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env) => _env = env;

        public void ConfigureServices(IServiceCollection services)
        {
            //Add config file as singleton
            services.AddScoped(v => new ConfigurationBuilder()
                .SetBasePath(_env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{_env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build());

            var config = services.BuildServiceProvider().CreateScope().ServiceProvider.GetService<IConfigurationRoot>();

            services.Configure<ConfigurationSettings>(config);
            services.AddTransient(s => s.GetService<IOptions<ConfigurationSettings>>().Value);

            services.AddDbContext<DatabaseContext>(options =>
                options.UseSqlServer(config.GetConnectionString("DefaultConnection")));

            // Hangfire
            GlobalConfiguration.Configuration.UseActivator(new HangfireJobActivator(services));
            GlobalConfiguration.Configuration.UseSqlServerStorage(config.GetDefaultConnection());
            services.AddHangfire(x => x.UseStorage(JobStorage.Current));

            // Repos and services
            ServiceExtensions.ConfigureServices(services, config);
            services.AddMemoryCache();

            services.AddCors();

            var mvc = services.AddMvc(v =>
            {
                // Order is important here!!
                v.Filters.Add<BasicAuthFilter>();
                v.Filters.Add<SessionTokenAuthenticateFilter>();
                v.Filters.Add<SessionTokenAuthorizeFilter>();
            });

            mvc.AddJsonOptions(
                opt =>
                {
                    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    opt.SerializerSettings.Converters.Add(new StringEnumConverter());
                    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IBaseRepository baseRepository)
        {
            baseRepository.InitializeDatabase().Wait();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            var config = app.ApplicationServices.CreateScope().ServiceProvider.GetConfig();       

            app.UseHttpsRedirection();

            app.UseHangfireServer();
            app.UseHangfireDashboard(
                "/hangfire",
                new DashboardOptions
                {
                    Authorization = new[] { new HangfireDashboardFilter() }
                });

            app.UseCors(options => options.WithOrigins(config.Cors.Origins).AllowAnyMethod().AllowAnyHeader());

            app.UseMvc();
        }
    }
}
