﻿using Foosball_project.Database;
using Foosball_project.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Repositories
{
    public class BaseRepository : IBaseRepository
    {
        private readonly DatabaseContext dbContext;

        public BaseRepository(DatabaseContext context)
        {
            dbContext = context;
        }

        public async Task InitializeDatabase()
        {
            await dbContext.Database.EnsureCreatedAsync();
        }
    }
}
