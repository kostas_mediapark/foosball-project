﻿using Foosball_project.Database;
using Foosball_project.Database.Interfaces;
using Foosball_project.Database.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Foosball_project.Repositories
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly DatabaseContext dbContext;

        public PlayerRepository(DatabaseContext context)
        {
            dbContext = context;
        }

        public async Task<IEnumerable<PlayerModel>> GetAll()
        {
            return await dbContext.Players.ToListAsync();
        }

        public async Task<PlayerModel> Get(string email)
        {
            return await dbContext.Players.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<PlayerModel> GetByUserName(string username)
            => await dbContext.Players.FirstOrDefaultAsync(x => x.Username.ToLowerInvariant() == username.ToLowerInvariant());

        public async Task<PlayerModel> Add(PlayerModel entity)
        {
            await dbContext.Players.AddAsync(entity);
            await dbContext.SaveChangesAsync();

            return entity;
        }

        public async Task<PlayerModel> GetById(int id)
        {
            return await dbContext.Players.FirstOrDefaultAsync(x => x.Id == id);
        }
    }

}
