﻿using Foosball_project.Database;
using Foosball_project.Database.Enums;
using Foosball_project.Database.Interfaces;
using Foosball_project.Database.Models;
using Hangfire;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Repositories
{
    public class LobbyRepository : ILobbyRepository
    {
        private const int MAX_PLAYERS_COUNT = 4;
        private const int DEFAULT_MATCH_DURATION_MINUTES = 25;

        private readonly DatabaseContext cx;

        public LobbyRepository(DatabaseContext context)
        {
            cx = context;
        }

        public async Task<LobbyStatusModel> AddPlayer(int userId)
        {
            var latestLobby = await cx.Lobby.OrderByDescending(x => x.Id)
                .FirstOrDefaultAsync();

            if (latestLobby.Status == LobbyStatus.Playing)
                throw new Exception("Game is already started");

            var players = await cx.Players.Where(x => x.Id == userId || x.TableId == latestLobby.Id).ToListAsync();

            var player = players.FirstOrDefault(x => x.Id == userId);

            player.TableId = latestLobby.Id;

            cx.Players.Update(player);

            await cx.SaveChangesAsync();

            if (players.Count == MAX_PLAYERS_COUNT)
                return await StartGame(latestLobby, players);

            return new LobbyStatusModel
            {
                Players = players.Select(x => x.Username),
                Status = latestLobby.Status,
                StartDate = latestLobby.StartDate,
                EndDate = latestLobby.EndDate
            };
        }

        public async Task<LobbyStatusModel> StartGame(LobbyModel latestLobby, List<PlayerModel> lobbyPlayers)
        {
            latestLobby.StartDate = DateTime.Now;
            latestLobby.Status = LobbyStatus.Playing;

            foreach (var player in lobbyPlayers)
            {
                player.Status = LobbyStatus.Playing;
            }

            cx.Lobby.Update(latestLobby);
            cx.Players.UpdateRange(lobbyPlayers);
            await cx.SaveChangesAsync();

            BackgroundJob.Schedule(() => ScheduledEndGame(latestLobby.Id), DateTime.Now.Add(new TimeSpan(0, DEFAULT_MATCH_DURATION_MINUTES, 0)));

            return new LobbyStatusModel
            {
                Players = lobbyPlayers.Select(x => x.Username),
                Status = latestLobby.Status,
                StartDate = latestLobby.StartDate,
                EndDate = latestLobby.EndDate
            };
        }

        public async Task ScheduledEndGame(int id = -1)
        {
            LobbyModel latestLobby;
            if (id == -1)
                latestLobby = await cx.Lobby.OrderByDescending(x => x.Id)
                .FirstOrDefaultAsync();
            else
                latestLobby = await cx.Lobby.FirstOrDefaultAsync(x => x.Id == id);

            if (latestLobby.Status != LobbyStatus.Playing)
                return;

            latestLobby.EndDate = DateTime.Now;
            latestLobby.Status = LobbyStatus.Ended;

            cx.Lobby.Update(latestLobby);
            await cx.SaveChangesAsync();

            await CreateNewLobby();
        }

        public Task<LobbyStatusModel> ClearLobby()
        {
            throw new NotImplementedException();
        }

        public async Task<LobbyStatusModel> Get()
        {
            LobbyModel latestLobby;

            if (cx.Lobby.Count() > 0)
                latestLobby = await cx.Lobby.OrderByDescending(x => x.Id)
                .FirstOrDefaultAsync();
            else
                latestLobby = await CreateNewLobby();

            var players = await cx.Players.Where(x => x.TableId == latestLobby.Id).ToListAsync();
            var model = new LobbyStatusModel
            {
                Status = latestLobby.Status,
                StartDate = latestLobby.StartDate,
                Duration = new TimeSpan(0, DEFAULT_MATCH_DURATION_MINUTES, 0),
                Players = players.Select(x => x.Username)
            };

            if (latestLobby.Status == LobbyStatus.Playing)
            {
                var endDate = latestLobby.StartDate.Value.Add(new TimeSpan(0, DEFAULT_MATCH_DURATION_MINUTES, 0));

                model.EndDate = endDate;
                model.TimeRemaining = endDate.Subtract(DateTime.UtcNow);
            }

            return model;
        }

        public async Task<LobbyStatusModel> RemovePlayer(int userId)
        {
            var latestLobby = await cx.Lobby.OrderByDescending(x => x.Id)
                .FirstOrDefaultAsync();

            if (latestLobby.Status == LobbyStatus.Playing)
                throw new Exception("Game is already started");

            var players = await cx.Players.Where(x => x.Id == userId || x.TableId == latestLobby.Id).ToListAsync();

            var player = players.FirstOrDefault(x => x.Id == userId);

            player.TableId = null;

            cx.Players.Update(player);

            await cx.SaveChangesAsync();

            players.Remove(player);

            return new LobbyStatusModel
            {
                Players = players.Select(x => x.Username),
                Status = latestLobby.Status,
                StartDate = latestLobby.StartDate,
                EndDate = latestLobby.EndDate
            };
        }

        private async Task<LobbyModel> CreateNewLobby()
        {
            var lobby = new LobbyModel
            {
                Status = LobbyStatus.Empty
            };

            await cx.Lobby.AddAsync(lobby);
            await cx.SaveChangesAsync();
            return await cx.Lobby.LastOrDefaultAsync();
        }
    }
}
