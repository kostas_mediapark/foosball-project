﻿using Foosball_project.Database;
using Foosball_project.Database.Interfaces;
using Foosball_project.Database.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Repositories
{
    public class SessionTokenRepository : ISessionTokenRepository
    {
        private readonly DatabaseContext cx;

        public SessionTokenRepository(DatabaseContext context)
        {
            cx = context;
        }

        public async Task<IEnumerable<SessionToken>> GetMany(int userId) => 
            await cx.SessionToken.Where(x => x.UserId == userId).ToListAsync();

        public async Task<SessionToken> Add(SessionToken token)
        {
            await cx.SessionToken.AddAsync(token);
            await cx.SaveChangesAsync();
            return token;
        }

        public async Task<SessionToken> Get(int userId) =>
            await cx.SessionToken.LastOrDefaultAsync(x => x.UserId == userId);

        public async Task<SessionToken> Get(int userId, string token) 
            => await cx.SessionToken.LastOrDefaultAsync(x => x.UserId == userId && x.Token == token);
    }
}
