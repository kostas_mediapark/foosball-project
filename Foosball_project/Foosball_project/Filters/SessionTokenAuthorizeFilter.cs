﻿using Foosball_project.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Filters
{
    public class SessionTokenAuthorizeFilter : IAsyncAuthorizationFilter
    {
        private readonly IServiceProvider _services;
        private FilterContext _context;
        private List<AuthorizeAttribute> AuthAttributes;
        private List<AuthorizeAttribute> ControllerAttributes;
        private List<AuthorizeAttribute> MethodAttributes;
        private bool MethodHasAllowAnonymous { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="loggerFactory"></param>
        public SessionTokenAuthorizeFilter(IServiceProvider services, ILoggerFactory loggerFactory)
        {
            _services = services;
            AuthAttributes = new List<AuthorizeAttribute>();
            ControllerAttributes = new List<AuthorizeAttribute>();
            MethodAttributes = new List<AuthorizeAttribute>();
        }

        /// <summary>
        /// Main authorization method
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
#pragma warning disable 1998
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            _context = context;

            GetAttributes();

            if (DoNeedAuthorize())
                Authorize();
        }
#pragma warning restore 1998

        private bool DoNeedAuthorize()
        {
            var method = ((ControllerActionDescriptor)_context.ActionDescriptor).MethodInfo;

            // Anonymous overrides controller attributes
            return MethodHasAllowAnonymous ? false : AuthAttributes.Any();
        }

        private void Authorize()
        {
            var passScheme = _context.HttpContext.User.Identity.IsAuthenticated && _context.HttpContext.User.Identity is SessionTokenIdentity;
            if (!passScheme)
                throw new UnauthorizedAccessException("Unauthorized user");

            var identity = (SessionTokenIdentity)_context.HttpContext.User.Identity;
            if (!identity.IsValid)
                throw new UnauthorizedAccessException("Unauthorized user");
        }

        /// <summary>
        /// Gets all AuthorizeAttribute attributes from controller and method
        /// </summary>
        private void GetAttributes()
        {
            var actionDescriptor = (ControllerActionDescriptor)_context.ActionDescriptor;

            var controller = actionDescriptor.ControllerTypeInfo;
            var controllerAttributes = controller.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            var method = actionDescriptor.MethodInfo;
            var methodAttributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            MethodHasAllowAnonymous = method.GetCustomAttributes(typeof(AllowAnonymousAttribute), false).Count() > 0;

            foreach (var atr in controllerAttributes)
            {
                AuthAttributes.Add((AuthorizeAttribute)atr);
                ControllerAttributes.Add((AuthorizeAttribute)atr);
            }

            foreach (var atr in methodAttributes)
            {
                AuthAttributes.Add((AuthorizeAttribute)atr);
                MethodAttributes.Add((AuthorizeAttribute)atr);
            }
        }
    }
}
