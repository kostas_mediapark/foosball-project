﻿using Hangfire.Dashboard;

namespace Foosball_project.Filters
{
    /// <summary>
    /// Hangfire authorization filter
    /// </summary>
    public class HangfireDashboardFilter : IDashboardAuthorizationFilter
    {
        /// <summary>
        /// Filter authorize
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Authorize(DashboardContext context)
        {
            return context.Request.RemoteIpAddress == "::1" || context.Request.RemoteIpAddress == "85.206.10.254";
        }
    }
}
