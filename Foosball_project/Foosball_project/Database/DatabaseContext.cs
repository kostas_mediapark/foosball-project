﻿using Foosball_project.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Foosball_project.Database
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions options) : base(options) { }

        public DbSet<PlayerModel> Players { get; set; }
        public DbSet<SessionToken> SessionToken { get; set; }
        public DbSet<LobbyModel> Lobby { get; set; }
    }
}
