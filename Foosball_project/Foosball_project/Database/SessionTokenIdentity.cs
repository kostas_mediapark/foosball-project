﻿using Foosball_project.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Foosball_project.Database
{
    public class SessionTokenIdentity : ClaimsIdentity
    {
        public const string SESSION_TOKEN_AUTH_SCHEME = "Bearer";
        public override string AuthenticationType => SESSION_TOKEN_AUTH_SCHEME;
        public override bool IsAuthenticated { get; }
        public override string Name { get; }
        public SessionToken SessionToken { get; set; }
        /// <summary>
        /// Did not expire yet
        /// </summary>
        public bool IsValid => SessionToken != null;

        public SessionTokenIdentity(SessionToken entity) : base()
        {
            SessionToken = entity;
            IsAuthenticated = entity != null;
            Name = null;
        }
    }
}
