﻿using Foosball_project.Database.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Foosball_project.Database.Interfaces
{
    public interface ISessionTokenRepository
    {
        Task<IEnumerable<SessionToken>> GetMany(int userId);
        Task<SessionToken> Add(SessionToken token);
        Task<SessionToken> Get(int userId, string token);
        Task<SessionToken> Get(int userId);
    }
}
