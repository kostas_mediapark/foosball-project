﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Database.Interfaces
{
    public interface IBaseRepository
    {
        Task InitializeDatabase();
    }
}
