﻿using Foosball_project.Database.Models;
using System.Threading.Tasks;

namespace Foosball_project.Database.Interfaces
{
    public interface ILobbyRepository
    {
        Task<LobbyStatusModel> Get();
        Task<LobbyStatusModel> AddPlayer(int userId);
        Task<LobbyStatusModel> RemovePlayer(int userId);
        Task<LobbyStatusModel> ClearLobby();
        Task ScheduledEndGame(int id = -1);
    }
}
