﻿using Foosball_project.Database.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Foosball_project.Database.Interfaces
{
    public interface IPlayerRepository
    {
        Task<IEnumerable<PlayerModel>> GetAll();
        Task<PlayerModel> Get(string email);
        Task<PlayerModel> GetByUserName(string username);
        Task<PlayerModel> Add(PlayerModel entity);
        Task<PlayerModel> GetById(int id);
    }
}
