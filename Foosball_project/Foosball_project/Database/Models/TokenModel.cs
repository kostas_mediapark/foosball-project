﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Database.Models
{
    public class TokenModel
    {
        public string Token { get; set; }

        public string Username { get; set; }

        public TokenModel() { }

        public TokenModel(Tuple<string, SessionToken> tuple)
        {
            Token = tuple.Item2.Token;
            Username = tuple.Item1;
        }
    }
}
