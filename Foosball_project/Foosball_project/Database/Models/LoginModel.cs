﻿namespace Foosball_project.Database.Models
{
    public class LoginModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public void CleanUpInputs()
        {
            Email = Email?.Trim();
            Password = Password?.Trim();
        }
    }
}
