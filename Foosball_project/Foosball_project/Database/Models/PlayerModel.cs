﻿using Foosball_project.Database.Enums;
using Foosball_project.Database.Interfaces;

namespace Foosball_project.Database.Models
{
    public class PlayerModel : IEntity
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public LobbyStatus Status { get; set; }
        public int? TableId { get; set; }
    }
}
