﻿using Foosball_project.Database.Interfaces;

namespace Foosball_project.Database.Models
{
    public class SessionToken : IEntity
    {
        public int Id { get; set; }

        public string Token { get; set; }

        public int UserId { get; set; }

        public SessionToken() { }
    }
}
