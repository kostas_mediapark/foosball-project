﻿using Foosball_project.Database.Enums;
using Foosball_project.Database.Interfaces;
using System;

namespace Foosball_project.Database.Models
{
    public class LobbyModel : IEntity
    {
        public int Id { get; set; }
        public LobbyStatus Status { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartDate { get; set; }

        public LobbyModel() { }
    }
}
