﻿using System.Linq;

namespace Foosball_project.Database.Models
{
    public class RegistrationModel
    {
        public string Email { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string RepeatPassword { get; set; }

        public bool IsValidInformation()
        {
            var isValid = true;

            if (!Password.Any(char.IsLetter) || !Password.Any(char.IsDigit) || Password.Length < 8)
                isValid = false;

            if (UserName.Length < 3 || UserName.Length > 12 || !UserName.All(char.IsLetter))
                isValid = false;

            return isValid;
        }
    }
}
