﻿using Foosball_project.Database.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Foosball_project.Database.Models
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Include)]
    public class LobbyStatusModel
    {
        public LobbyStatus Status { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? Duration { get; set; }
        public TimeSpan? TimeRemaining { get; set; }
        public DateTime? StartDate { get; set; }
        public IEnumerable<string> Players { get; set; }

        public LobbyStatusModel() { }
    }
}
