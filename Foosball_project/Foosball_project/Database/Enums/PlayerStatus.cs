﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Database.Enums
{
    public enum PlayerStatus
    {
        Off,
        InQueue,
        Playing
    }
}
