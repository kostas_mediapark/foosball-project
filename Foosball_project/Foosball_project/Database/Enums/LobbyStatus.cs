﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Database.Enums
{
    public enum LobbyStatus
    {
        Empty = 0,
        Waiting = 1,
        Playing = 2,
        Ended = 3
    }
}
