﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Attributes
{
    public class SessionTokenAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Default and only constructor
        /// </summary>
        /// <param name="tokenType"></param>
        /// <param name="requiredPermissions"></param>
        public SessionTokenAuthorizeAttribute() : base()
        {
        }
    }
}

