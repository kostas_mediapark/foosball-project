﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foosball_project.Attributes
{
    /// <summary>
    /// Basic Auth attribute
    /// </summary>
    public class BasicAuthAttribute : Attribute
    {
    }
}
